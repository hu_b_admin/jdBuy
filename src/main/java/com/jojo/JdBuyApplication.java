package com.jojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JdBuyApplication {
    public static void main(String[] args) {
        SpringApplication.run(JdBuyApplication.class, args);
    }
}
