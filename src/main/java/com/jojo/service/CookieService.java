package com.jojo.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jojo.entity.BasicInfo;
import com.jojo.entity.Result;
import com.jojo.mapper.BasicInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CookieService {

    @Autowired
    private BasicInfoMapper basicInfoMapper;

    /**
     * 添加/更新cookie
     */
    public Result submitCookie(BasicInfo basicInfo) {
        if (StrUtil.isBlank(basicInfo.getPhone())) {
            return Result.error(-1, "手机号不能为空");
        }
        if (StrUtil.isBlank(basicInfo.getCookie())) {
            return Result.error(0, "cookie不能为空");
        }
        basicInfo.setUpdateTime(new Date());
        QueryWrapper<BasicInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", basicInfo.getPhone());
        List<BasicInfo> basicInfoList = basicInfoMapper.selectList(queryWrapper);
        if (!basicInfoList.isEmpty()) {
            basicInfo.setId(basicInfoList.get(0).getId());
            basicInfoMapper.updateById(basicInfo);
            return Result.success(0, "更新成功");
        } else {
            basicInfo.setId(UUID.randomUUID().toString());
            basicInfoMapper.insert(basicInfo);
            return Result.success(0, "添加成功");
        }
    }
}
