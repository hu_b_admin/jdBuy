/*
 Navicat Premium Data Transfer

 Source Server         : 本地localhost-root-123456
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : jd_buy

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 13/01/2022 09:50:31
*/
DROP DATABASE IF EXISTS `jd_buy`;
create database ` jd_buy
` default
character set utf8mb4 collate utf8mb4_general_ci;

USE
`jd_buy
` ;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for basic_info
-- ----------------------------
DROP TABLE IF EXISTS `basic_info`;
CREATE TABLE ` basic_info `
(
    `
    id` varchar(255) CHARACTER
    SET
    utf8
    COLLATE
    utf8_general_ci
    NOT
    NULL, `
    phone` varchar(255) CHARACTER
    SET
    utf8
    COLLATE
    utf8_general_ci
    NULL
    DEFAULT
    NULL, `
    cookie` varchar(5000) CHARACTER
    SET
    utf8
    COLLATE
    utf8_general_ci
    NULL
    DEFAULT
    NULL, `
    pid` varchar(255) CHARACTER
    SET
    utf8
    COLLATE
    utf8_general_ci
    NULL
    DEFAULT
    NULL, `
    update_time` timestamp(0) NULL DEFAULT
    NULL, `
    time` timestamp(0)        NULL DEFAULT
    NULL,
    PRIMARY KEY (` id `) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
