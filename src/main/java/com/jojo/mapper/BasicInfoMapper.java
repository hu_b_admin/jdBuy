package com.jojo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jojo.entity.BasicInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BasicInfoMapper extends BaseMapper<BasicInfo> {
}
