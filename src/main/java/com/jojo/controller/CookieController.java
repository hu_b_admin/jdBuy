package com.jojo.controller;

import com.jojo.entity.BasicInfo;
import com.jojo.entity.Result;
import com.jojo.service.CookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("cookie")
public class CookieController {
    @Autowired
    private CookieService cookieService;

    @GetMapping("submitCookiePage")
    public String updateCookiePage() {
        return "submitCookiePage";
    }

    @PostMapping("submitCookie")
    @ResponseBody
    public Result submitCookie(@RequestBody BasicInfo basicInfo) {
        return cookieService.submitCookie(basicInfo);
    }
}
